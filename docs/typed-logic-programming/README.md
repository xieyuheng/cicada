# typed logic programming

This directory contains logic programming examples  
taken from classical logic programming textbooks.

Including :
- << Clause and effect: Prolog programming for the working programmer >>  
  by William F. Cloeksin, 1997.
