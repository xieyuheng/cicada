#![feature (bind_by_move_pattern_guards)]
#![feature (uniform_paths)]

#![allow (dead_code)]

mod micro;
mod yoneda;
